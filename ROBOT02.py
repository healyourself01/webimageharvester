"""
IMAGE DOWNLOADER
Downloads images from google and bing image search
and saves them in the given folder

download_images_bing
--------------------
outdir="D:\Andomeda Robot\IMAGE_LIB_WEB", 
word="Cool Short Hairstyles for Men", 
minHight=100, 
minWidth=50, 
img_type="photo", 
img_layout="tall", 
maxImages = 10
sample
# download_images_bing(outdir="D:\Andomeda Robot\IMAGE_LIB_WEB", word="Cool Short Hairstyles for Men", minHight=100, minWidth=50, img_type="photo", img_layout="tall", maxImages = 10)
# quit()

     

"""

import os
import csv
import time
import json
import urllib
import urllib2, shutil
from urllib3 import *
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementNotVisibleException,StaleElementReferenceException,NoSuchElementException,WebDriverException
import APP_CONFIG
from APP_CONFIG import *
from django.utils import encoding

#setting the headers for the urllib browser
header = {'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}
profile = webdriver.FirefoxProfile(FIREFOX_PROFILE)
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)

def close_browser():
    driver.quit()

def convert_unicode_to_string(x):
    """
    >>> convert_unicode_to_string(u'ni\xf1era')
    'niera'
    """
    return encoding.smart_str(x, encoding='ascii', errors='ignore')


""" ------------BING IMAGE DOWNLOADER MAIN FUNCTION----------------"""


def download_bing_image_search_url(word, bing_url, outdir, maxImages):
    print "URL:", bing_url
    """-----------------------FOLDER CREATION----------------------------"""
    # creating directory for the search key word in the drive and changing the directory
    # Create the folder if it doesn't exist
    # creating directory
    SAVE_DIR = os.path.join(outdir, "01_original_images")
    if not os.path.exists(SAVE_DIR):
        os.makedirs(SAVE_DIR)
    else:
        print "This URL already processed. Skipping.."
        return
        # shutil.rmtree(SAVE_DIR)
        # time.sleep(05)
        # os.makedirs(SAVE_DIR)
    os.chdir(SAVE_DIR)
    """-----------------------SCROLL DOWN FETCH IMAGE URLS----------------------------"""
    try:
        driver.get(bing_url)
    except WebDriverException:
        pass

    # scrolls to the end of the page
    scroll_count = 0
    # by default we get 35 images. so need to scroll down only when we need more than 35 images
    while scroll_count <= 50 and maxImages > 35:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
        try:
            driver.find_element_by_class_name('btn_seemore').click()
            print ("clicked on more button.")
        except NoSuchElementException:
            print "NoSuchElementException"
            pass
        except ElementNotVisibleException:
            print "ElementNotVisibleException"
            pass
        except WebDriverException:
            print "WebDriverException"
            pass
        finally:
            scroll_count += 1

    """-----------------------DOWNLOAD THE IMAGES----------------------------"""
    # Get all the images and print them
    TOTAL_IMAGES = driver.find_elements_by_css_selector("a.iusc")
    print "Total Images Found:", len(TOTAL_IMAGES)
    counter_image_download = 0
    for ele in driver.find_elements_by_css_selector("a.iusc"):
        data = convert_unicode_to_string(ele.get_attribute("m"))
        json_data = json.loads(data)
        image = json_data["murl"]
        # image_name = convert_unicode_to_string(json_data["purl"])+"_" + convert_unicode_to_string(image).split("/")[-1]
        image_name = convert_unicode_to_string(image).split("/")[-1]
        # write the image URL
        f = open('%s.txt' % word, 'a+')
        f.write((image + '\n'))
        f.close()
        # downloading the images into specific directory
        try:
            if counter_image_download > maxImages:
                continue

            req = urllib2.Request(image, headers={'User-Agent': header})
            print ("Downloading Image:%d:%s" % (counter_image_download, image_name))
            raw_img = urllib2.urlopen(req).read()
            File = open(os.path.join(SAVE_DIR + "\\" + image_name), "wb")
            File.write(raw_img)
            File.close()
            counter_image_download = counter_image_download + 1
        except:
            d = open('error.txt', 'a+')
            d.write((image + '\n'))
            d.close()


BING_BASE = "https://www.bing.com/images/search?q="
"&qft=+filterui:imagesize-custom_300_500+filterui:aspect-tall+filterui:photo-photo&FORM=R5IR22"
BING_QUERY = "&qft="
BING_Image_SIZE = "+filterui:imagesize-custom_HEIGHT_WIDTH"
BING_LAYOUT = "+filterui:aspect-VALUE"
BING_TYPE = "+filterui:photo-VALUE"
BING_TAIL = "&FORM=R5IR22"

def download_images_bing(word,outdir, minHight, minWidth, img_type, img_layout, maxImages):
    keyword = word.replace(" ", "+")
    image_size = BING_Image_SIZE.replace("HEIGHT", str(minHight)).replace("WIDTH", str(minWidth))
    img_layout = BING_LAYOUT.replace("VALUE",img_layout)
    img_type = BING_TYPE.replace("VALUE", img_type)
    bing_image_search_url = BING_BASE+keyword+BING_QUERY+image_size+img_layout+img_type+BING_TAIL
    outdir = os.path.join(outdir,word.lower().replace(" ","_"))
    download_bing_image_search_url(word=word, bing_url=bing_image_search_url, outdir=outdir, maxImages=maxImages)


# download_images_bing(outdir="D:\Andomeda Robot\IMAGE_LIB_WEB", word="Cool Short Hairstyles for Men", minHight=100, minWidth=50, img_type="photo", img_layout="tall", maxImages = 10)
# quit()




