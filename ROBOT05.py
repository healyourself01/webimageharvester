from pynput import keyboard
import subprocess, time
import os, PIL
from PIL import Image
from getch import getch, pause

IMAGE_FILE_EXTENSIONS = ('.jpg', '.jpeg', ".png")
KEY_PRESSED = keyboard.Key.esc

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    # print('{0} released'.format(
    #     key))
    global KEY_PRESSED
    KEY_PRESSED = key
    return False


def create_dir(dir_temp):
    path = os.path.join(DIR_BASE, dir_temp)
    if not os.path.isdir(path):
        os.mkdir(path)
"""
------------------------------------------------------------------
is_image(filename)
checks whether the file is image and returns True or False
------------------------------------------------------------------
"""


def is_image(filename):
    if os.path.splitext(filename)[1].lower() in IMAGE_FILE_EXTENSIONS:
        return True
    else:
        return False


#
#   VALIDATE IMAGES AND MOVE TO CORRESPONDING FOLDERS
#   ROBOT05
#
#
APP_DIR = "eid _mehadi_designs"
DIR_BASE = "C://Users//Ajay//OneDrive//production_robot_apps" + '//' + APP_DIR
ORIGINAL_DIR_STRING = "01_original_images"
VALID_DIR_STRING = "02_valid_images"
WATERMARK_DIR_STRING = "03_watermark_images"
TEXT_DIR_STRING = "03_text_images"
FACE_DIR_STRING = "03_face_images"
APP_NAME = "Simple_Mehndi_Designs"

VALID_IMAGES_LIMIT = 16

create_dir(VALID_DIR_STRING)
create_dir(WATERMARK_DIR_STRING)
create_dir(TEXT_DIR_STRING)
create_dir(FACE_DIR_STRING)

# Collect events until released
# with keyboard.Listener(
#         on_press=on_press,
#         on_release=on_release) as listener:
#     listener.join()


original_images_list = os.listdir(os.path.join(DIR_BASE, ORIGINAL_DIR_STRING))

# Change the current dir
os.chdir(os.path.join(DIR_BASE, ORIGINAL_DIR_STRING))
count = 0
count_valid = 0
for item in original_images_list:
    if is_image(item):
        count = count + 1
        try:
            im = Image.open(item)
        except:
            print "unable to open:", item

        im.show()
        VALID_IAMGE_DIR = os.path.join(DIR_BASE, VALID_DIR_STRING)
        WATERMARK_IMAGE_DIR = os.path.join(DIR_BASE, WATERMARK_DIR_STRING)
        TEXT_IMAGE_DIR = os.path.join(DIR_BASE, TEXT_DIR_STRING)
        FACE_IMAGE_DIR = os.path.join(DIR_BASE, FACE_DIR_STRING)
        # new_img_name = "Image_" + str(count) + ".jpg"
        new_img_name = "img_" + str(count) +"_"+APP_NAME + ".jpg"

        # Collect events until released
        with keyboard.Listener(
                on_press=on_press,
                on_release=on_release) as listener:
            listener.join()

        if KEY_PRESSED == keyboard.Key.f9:
            print "{0} moved to Valid Images".format(item)
            new_img_path = os.path.join(VALID_IAMGE_DIR, new_img_name)
            im.save(new_img_path)
            count_valid += 1


        if KEY_PRESSED == keyboard.Key.f10:
            print "{0} moved to Watermark Images".format(item)
            new_img_path = os.path.join(WATERMARK_IMAGE_DIR, new_img_name)
            im.save(new_img_path)

        if KEY_PRESSED == keyboard.Key.f11:
            print "{0} moved to Text Images".format(item)
            new_img_path = os.path.join(TEXT_IMAGE_DIR, new_img_name)
            im.save(new_img_path)

        if KEY_PRESSED == keyboard.Key.f12:
            print "{0} moved to Face Images".format(item)
            new_img_path = os.path.join(FACE_IMAGE_DIR, new_img_name)
            im.save(new_img_path)

        im.close()

#         TASKKILL /F /IM Microsoft.Photos.exe
        Photos_kill_cmd = ["TASKKILL", "/F", "/IM", "Microsoft.Photos.exe"]
        subprocess.Popen(Photos_kill_cmd, stdout=subprocess.PIPE, shell=True)
        time.sleep(1)
        # --------------------------Exit when valid image limit reached
        if count_valid > VALID_IMAGES_LIMIT:
            print "Valid Image Limit Reached."
            quit()


quit()


