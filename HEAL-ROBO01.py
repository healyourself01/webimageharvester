import os
import json
from os.path import join, getsize
import fnmatch
import xlsxwriter
from openpyxl import load_workbook
BASE_FOLDER_PATH = "C://Users//Ajay//Google Drive//Healing Finalized Audios"
albums_enabled = ("Diabetes", "Introduction","Acne","Back_Healing",
                  "Blood_Pressure","Constipation","Diarrhea",
                  "Eyes","Fever", "Hair","Heart",
                  "Meditations","Sleep")
IMAGE_FILE_EXTENSIONS = ('.jpg', '.jpeg', ".png")
AUDIO_FILE_EXTENSIONS = ('.mp3', '.m4a', ".wav")
ALBUM_ID = 700

import os
import json

songs = []
items = []
item = {}
song = {}

"""
----------------------------ALBUM ID--------------------------------------
is_image(filename)
checks whether the file is image and returns True or False
----------------------------ALBUM ID--------------------------------------
"""


def get_album_id():
    global ALBUM_ID
    ALBUM_ID = ALBUM_ID + 1
    return str(ALBUM_ID)

"""
------------------------------------------------------------------
is_image(filename)
checks whether the file is image and returns True or False
------------------------------------------------------------------
"""


def is_image(filename):
    if os.path.splitext(filename)[1].lower() in IMAGE_FILE_EXTENSIONS:
        return True
    else:
        return False
"""
------------------------------------------------------------------
is_audio(filename)
checks whether the file is audio and returns True or False
------------------------------------------------------------------
"""


def is_audio(filename):
    if os.path.splitext(filename)[1].lower() in AUDIO_FILE_EXTENSIONS:
        return True
    else:
        return False


"""
------------------------------------------------------------------
get_album_details_from_excel(song_id)
Find the song details from excel and return 
------------------------------------------------------------------
"""


def get_album_details_from_excel(song_id):
    # Load the workbook
    wb = load_workbook(filename="album_details_excel.xlsx")
    ws = wb['album_details']

    try:
        song_info = {"title": "", "short-desc": "", "long-desc": ""}
        for row in ws.iter_rows(range_string="B2:Z200"):
            id = str(row[2].value)
            title = ""
            short_desc = ""
            long_desc = ""
            if song_id == id:
                title = row[4].value
                short_desc = row[5].value
                long_desc = row[5].value
            song_info = {"title": title, "short-desc":short_desc, "long-desc": long_desc}
    except IndexError:
            print ("Index Error. Please check.")
    finally:
        wb.close()
        return song_info

"""
------------------------------------------------------------------
get_album_cover (album_dir)
Find the cover Image 
------------------------------------------------------------------
"""
def get_album_cover_image(BASE_FOLDER_PATH, album_dir):
    for file in os.listdir(os.path.join(BASE_FOLDER_PATH, album_dir)):
        # if is_image(file) and "album" in file:
        if is_image(file):
            return file
    return "NotFound"


def list_files(startpath):
    with open("albums.json", "w") as f_output:
        # Start from the first cell below the headers.
        row = 1
        col = 1
        for root, dirs, files in os.walk(startpath):
            global songs, items, item
            songs = []
            level = root.replace(startpath, '').count(os.sep)
            indent = '//t' * 1 * (level)
            output_string = '{}{}/'.format(indent, os.path.basename(root))
            # print(output_string)
            # f_output.write(output_string + '//n')
            subindent = '//t' * 1 * (level + 1)
            # for file in files:
            album_id = get_album_id()
            song_count = 0
            for file in fnmatch.filter(files, "*"):
                global song
                album_image = ""
                # output_string = '{}{}'.format(subindent, file)
                if is_image(file):
                    album_image = file
                if is_audio(file):
                    song_count = song_count + 1
                    song_id = album_id + "." +str(song_count)
                    song = {'song_id': song_id, 'song_name': str(file)}
                    songs.append(song)
            #-------------------WRITE TO EXCEL---------------------------
                    worksheet.write_string(row, col, os.path.basename(root)) #Album Name
                    worksheet.write_string(row, col+1, album_id) #Album id
                    worksheet.write_string(row, col+2, song_id) #song id
                    worksheet.write_string(row, col+3, str(file)) #song file name
                    row = row + 1
                # print(output_string)
                # f_output.write(output_string + '//n')
            item = {"album_id": album_id, 'album_name': os.path.basename(root),
                    "album_image":album_image, 'album_songs': songs}
            items.append(item)
        workbook.close()
    # f_output.write(json.dumps(items, indent=4))
#----------------------------------------------
#          Get Songs List from Language Dir
#----------------------------------------------

def get_songs_list(BASE_FOLDER_PATH, album_dir, language_dir, album_id):
        if os.path.isdir(os.path.join(BASE_FOLDER_PATH, album_dir, language_dir)) is True:  # check if the file is dir
            song_count = 0
            songs = []
            for album_item in os.listdir(os.path.join(BASE_FOLDER_PATH, album_dir, language_dir)):  # Loop through files
                if is_audio(album_item):
                    song_count = song_count + 1
                    song_id = album_id + "." +language_dir+"."+ str(song_count)
                    song_info = get_album_details_from_excel(song_id)
                    song = {'song_id': song_id,
                            'song_status': True,
                            'song_name': str(album_item),
                            'song_title': str(album_item),
                            'song_desc_short': song_info['short-desc'],
                            'song_desc_long': song_info['long-desc']}
                    songs.append(song)
            return songs
        else:
            return []


# print get_album_details_from_excel("701.1")
# quit()

workbook = xlsxwriter.Workbook("album_details_excel.xlsx")
worksheet = workbook.add_worksheet("album_details")
# Add a bold format to use to highlight cells.
bold = workbook.add_format({'bold': 1})

# Write Header for excel
worksheet.write("A1", "SNo", bold)
worksheet.write("B1", "Album Name", bold)
worksheet.write("C1", "Album ID", bold)
worksheet.write("D1", "Song ID", bold)
worksheet.write("E1", "Song File Name", bold)
worksheet.write("F1", "Publish Status", bold)
worksheet.write("G1", "Song Title", bold)
worksheet.write("H1", "Song Short Desc", bold)
worksheet.write("I1", "Song Long Desc", bold)
worksheet.write("J1", "Type", bold)
worksheet.write("K1", "URL", bold)

# list_files(BASE_FOLDER_PATH)
global songs, items, item
songs = []
row = 1
col = 1


for album_dir in os.listdir(BASE_FOLDER_PATH): #Loop through the Albums
    if album_dir not in albums_enabled:
        continue
    if os.path.isdir(os.path.join(BASE_FOLDER_PATH, album_dir)): #check if the file is dir
        album_id = get_album_id()
        item = {"album_id": album_id,
                "album_name": album_dir,
                "album_status": True,
                "album_cover": get_album_cover_image(BASE_FOLDER_PATH, album_dir),
                "Telugu_songs": get_songs_list(BASE_FOLDER_PATH,album_dir,"Telugu",album_id),
                "English_songs": get_songs_list(BASE_FOLDER_PATH,album_dir,"English",album_id),
                "Hindi_songs": get_songs_list(BASE_FOLDER_PATH,album_dir,"Hindi",album_id)
                }
    items.append(item)

output_file = open("albums_generated.json", "w")
output_file.write(json.dumps(items, indent=4))
output_file.close()
quit()

for album_dir in os.listdir(BASE_FOLDER_PATH): #Loop through the Albums
    if album_dir <> "Diabetes":
        continue
    if os.path.isdir(os.path.join(BASE_FOLDER_PATH, album_dir)): #check if the file is dir
        album_id = get_album_id()
        item = {"album_id": album_id, 'album_name': album_dir}
        for language_dir in os.listdir(os.path.join(BASE_FOLDER_PATH, album_dir)): # Loop through Language Dirs
            if os.path.isdir(os.path.join(BASE_FOLDER_PATH,album_dir,language_dir)) is True: #check if the file is dir
                song_count = 0
                for album_item in os.listdir(os.path.join(BASE_FOLDER_PATH,album_dir, language_dir)): #Loop through files
                    global song
                    album_image = ""
                    # if is_image(album_item):
                    #     album_image = file
                    if is_audio(album_item):
                        song_count = song_count + 1
                        song_id = album_id + "." + str(song_count)
                        song = {'song_id': song_id, 'song_name': str(album_item)}
                        songs.append(song)
                        # -------------------WRITE TO EXCEL---------------------------
                        worksheet.write_string(row, col, album_dir)  # Album Name
                        worksheet.write_string(row, col + 1, album_id)  # Album id
                        worksheet.write_string(row, col + 2, song_id)  # song id
                        worksheet.write_string(row, col + 3, str(file))  # song file name
                        row = row + 1
            elif is_image(language_dir): #Get the Album Image
                item['album_image'] = language_dir
                continue
            item[language_dir + "_songs"] = songs
            songs = []
            items.append(item)
            item = {}
workbook.close()



# https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/Blood_Pressure/Telugu/01-Blood%20Pressure-Telugu-4min.mp3
#  ttps://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/Blood_Pressure/Telugu/01-Blood%Pressure-Telugu-4min.mp3
#




