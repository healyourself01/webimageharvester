from types import ModuleType
import numpy as np
import os
from PIL import Image
import fnmatch



def search_submodules(module, identifier):
    assert isinstance(module, ModuleType)
    ret = None
    for attr in dir(module):
        if attr == identifier:
            ret = '.'.join((module.__name__, attr))
            break
        else:
            submodule = getattr(module, attr)
            if isinstance(submodule, ModuleType):
                ret = search_submodules(submodule, identifier)
    return ret

if __name__ == '__main__':
    import cv2
    print (cv2.__version__)




def captch_ex(file_name):
    img = cv2.imread(file_name)
    img2 = img.copy()
    face_cascade_path = 'haarcascade_eye.xml'
    img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    image_pil = Image.open(file_name).convert('L')
    image = np.array(image_pil, 'uint8')
    eye_cascade = cv2.CascadeClassifier(face_cascade_path)
    faces = eye_cascade.detectMultiScale(image)
    for (ex, ey, ew, eh) in faces:
        cv2.rectangle(img, (ex, ey), (ex + ew, ey + eh), (255, 0, 0))

    cv2.imshow('img', img)
    cv2.waitKey(0)



















DIR_IMAGE_LIB = "F:\webimageharvester"
for root, dir, files in os.walk(DIR_IMAGE_LIB):
    for item in fnmatch.filter(files, "*.jpg"):
        file_name = root+"\\"+item
        captch_ex(file_name)