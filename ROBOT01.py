import os
import csv
import time
import json
import urllib
import urllib3
from urllib3 import *
# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementNotVisibleException,StaleElementReferenceException,NoSuchElementException
import APP_CONFIG
from APP_CONFIG import *

BASE_url = 'http://www.google.co.in'
OUTPUT_DIR = "C:/Users/mindgame/Google Drive/APK_Downloads"

profile = webdriver.FirefoxProfile(FIREFOX_PROFILE)
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)

#we can specify more number of key words
searchterm = "top hairstyles for men 2017"
driver.get(BASE_url)
search=driver.find_element_by_name("q")

#enters the keyword into google search
search.send_keys(searchterm)
search.submit()
wait.until(EC.presence_of_element_located((By.ID, "resultStats")))

#gets the image button of url we can directly implement click on element but tried this way and implemented click in
#later part of the program
strin=driver.find_element_by_css_selector('div.hdtb-mitem:nth-child(2) > a:nth-child(1)').get_attribute('href')
time.sleep(3)
print (strin)
driver.get(strin)

#scrolls to the end of the page
second = 0
while second <= 10:
  driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
  time.sleep(3)
  try:
      driver.find_element_by_css_selector('#smb').click()
  except ElementNotVisibleException:

    second = second+1
#creating directory for the search key word in the drive and changing the directory
if not os.path.exists('\%s' %searchterm):
    os.makedirs('\%s' %searchterm)
os.chdir('\%s' %searchterm)

count=0
count1=0
list2=[]

#getting all the domain information from the images in google search
for ele in driver.find_elements_by_xpath("//div[@class='rg_meta']"):

        #domain info
        domain=json.loads(ele.get_attribute('innerHTML'))["ru"]

        #eleminating youtube from domain list
        you= str(domain).find("youtube")
        dup = 0
        if (you) != -1:
            count1 += 1
        else:
            #eleminating duplicates in the domain info list
            for ur in list2:
                if domain == ur:
                    dup += 1
            if dup == 0:
                list2.append(domain)
                count += 1
                print (domain)
                #writing each domain into the text file
                txt = open("%s.txt" %searchterm,'a+')
                txt.write((domain+'\n'))
                txt.close()

#setting the headers for the urllib browser
header = {'User-Agent': "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}

#getting all the image urls from domain search in advanced image search for each domain
for ele1 in list2:

    driver.get(BASE_url)
    search = driver.find_element_by_name("q")

    #here i hard coded men further going we need to implement this to be dynamic
    search.send_keys('men site:%s' % ele1)
    search.submit()
    time.sleep(2)
    try:
        #here i implemented click on the all images element
        driver.find_element_by_css_selector('div.hdtb-mitem:nth-child(2) > a:nth-child(1)').click()
        time.sleep(1)
    except NoSuchElementException:
        pass

    #scrolling process
    second = 0
    while second <= 5:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        try:
            driver.find_element_by_css_selector('#smb').click()
        except (ElementNotVisibleException,NoSuchElementException):
            second = second + 1

    #getting all the image urls
    for ele in driver.find_elements_by_xpath("//div[@class='rg_meta']"):

        #image url
        image = json.loads(ele.get_attribute('innerHTML'))["ou"]
        #image type
        imgtype = json.loads(ele.get_attribute('innerHTML'))["ity"]
        #site name
        folder = json.loads(ele.get_attribute('innerHTML'))["isu"]

        #creating directory
        if not os.path.exists("/%s/%s/" %(searchterm ,folder)):
            os.makedirs('/%s/%s/' %(searchterm ,folder))
            counter = 0
        os.chdir('/%s/%s/' %(searchterm ,folder))

        #writing each url into text file for further reference
        f = open('%s.txt' % folder, 'a+')
        f.write((image+'\n'))
        f.close()

        #downloading the images into specific directory
        try:
            #raw_img = urllib.request.urlretrieve(image,searchterm+"_"+str(counter)+"."+imgtype)
            #counter = counter + 1
            req = urllib2.Request(image, headers={'User-Agent': header})
            raw_img = urllib2.urlopen(req).read()
            File = open(os.path.join('/%s/%s/' %(searchterm ,folder), searchterm + "_" + str(counter) + "." + imgtype), "wb")
            File.write(raw_img)
            File.close()
            counter = counter + 1
        except:
            d = open('error.txt' ,'a+')
            d.write((image + '\n'))
            d.close()

driver.quit()
print(count)
#print(count1)