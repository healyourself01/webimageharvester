# pip install tornado
import tornado.ioloop
import tornado.web
import os
import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid



from tornado.options import define, options



define("port", default=80, help="run on the given port", type=int)




class Application(tornado.web.Application):

    def __init__(self):

        handlers = [

            (r"/", MainHandler),

            (r"/websocket", EchoWebSocket)
        ]

        settings = dict(

            blog_title=u"Tornado Blog",

            template_path=os.path.join(os.path.dirname(__file__), "templates"),

            static_path=os.path.join(os.path.dirname(__file__), "static"),

            # ui_modules={"Entry": EntryModule},

            xsrf_cookies=True,

            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",

            login_url="/auth/login",

            debug=True,

        )

        super(Application, self).__init__(handlers, **settings)


class EchoWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        print "Web Socket Message Received."
        self.write_message(u"You said: " + message)

    def on_close(self):
        print("WebSocket closed")


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        # self.write("Hello, world")
        self.render("index.html", students=range(20))

    def post(self, *args, **kwargs):
        print "Post called."
        print self.request.body
        self.write("Data received.")


def main():

    tornado.options.parse_command_line()
    print "Listening on port:{0}".format(80)
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()