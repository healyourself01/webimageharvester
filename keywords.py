import os, winsound
import csv
import time
import json
import xlsxwriter
import APP_CONFIG
from APP_CONFIG import FIREFOX_PROFILE

# Imports the Google Cloud client library
from google.cloud import datastore
from oauth2client.client import GoogleCredentials
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import ElementNotVisibleException,\
    StaleElementReferenceException,NoSuchElementException, WebDriverException

BASE_url = 'http://www.google.com'
OUTPUT_DIR = "C:/Users/Ajay/Google Drive/APK_Downloads"
profile = webdriver.FirefoxProfile(FIREFOX_PROFILE)
profile.set_preference('browser.download.folderList', 2)
profile.set_preference('browser.download.manager.showWhenStarting', False)
profile.set_preference('browser.download.dir', OUTPUT_DIR)
profile.set_preference('browser.helperApps.neverAsk.saveToDisk', ("text/css,application/json,text/plain"))
driver = webdriver.Firefox(profile)
wait = WebDriverWait(driver, 10)

from django.utils import encoding
# ---------UNICODE to ASCII conversion
def convert_unicode_to_string(x):
    """
    >>> convert_unicode_to_string(u'ni\xf1era')
    'niera'
    """
    return encoding.smart_str(x, encoding='ascii', errors='ignore')

def getValueFromPage(element, value):
    try:
        return convert_unicode_to_string(element.text).decode('utf-8').replace("\\n","")
    except:
        print "Error reading value:"
        print element
        return "Error Reading Page"

def scroll_to_bootom():
    scroll_count = 0
    "Scroll down dot the bootom of the page to capture all the sugg"
    while scroll_count <= 10:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)
        scroll_count = scroll_count + 1
        try:
            driver.find_element_by_css_selector('a.btn_seemore').click()
            print ("clicked on more button.")
            "When you see more button break the loop"
            break
            return
        except NoSuchElementException:
            pass
        except ElementNotVisibleException:
            pass
        except WebDriverException:
            pass
        finally:
            scroll_count += 1

# searchterms=("hairstyles for men", "hairstyles for woman","tatoo designs" ,"mehndi designs","rangoli designs", "blouse designs", "saree designs", "kurti designs","tshirt designs")
searchterms=("saree designs", "blouse designs", "mehndi")

os.chdir("D:\Andomeda Robot\webimageharvester")
workbook = xlsxwriter.Workbook("app_names.xlsx")
worksheet = workbook.add_worksheet("APP_NAME_SUGG_SH01")
# Add a bold format to use to highlight cells.
bold = workbook.add_format({'bold': 1})

# Write Header for excel
worksheet.write("A1", "SNo",bold)
worksheet.write("B1", "Search_Term",bold)
worksheet.write("C1", "App_Suggestion_original",bold)
worksheet.write("D1", "App_Suggestion_Edited",bold)
worksheet.write("E1", "Length",bold)
worksheet.write("F1", "Source",bold)
worksheet.write("G1", "Type",bold)
worksheet.write("F1", "URL",bold)


# Start from the first cell below the headers.
row = 1
col = 1

for searchterm in searchterms:
    base_url = 'https://www.google.co.in/search?q='+searchterm
    # -----------------------------------GOOGLE------------------------------------------
    driver.get(base_url)
    Search_Term = searchterm
    Source = "Google"

    f = open('%s.txt' % searchterm, 'w')

    # Get all the -----PAGE TITLES
    for ele in driver.find_elements_by_css_selector('h3 a'):
        text = str(getValueFromPage(ele, 'innerText')).strip()
        try:
            f.write((text + '\n'))
            col = 1
            worksheet.write_string(row, col, Search_Term)
            worksheet.write_string(row, col+1, text) #App Sugg
            # 'Remove all numbers in edited string'
            worksheet.write_string(row, col+2, text.translate(None, '0123456789')) #App Sugg - Edited
            worksheet.write_number(row, col+3, int(len(text)))  #Length
            worksheet.write_string(row, col+4, "Google")  #Source
            worksheet.write_string(row, col+5, "Google-Page-Title")  #Type
        except UnicodeEncodeError:
            print ele
            print text
            print "Error occurred!"
        finally:
            row = row + 1

    # Get all the -KEYWORD SUGGESTIONS
    for ele in driver.find_elements_by_css_selector('#brs a'):
        text = str(getValueFromPage(ele, 'innerText')).strip()
        try:
            f.write((text + '\n'))
            worksheet.write_string(row, col, Search_Term)
            worksheet.write_string(row, col+1, text) #App Sugg
            # 'Remove all numbers in edited string'
            worksheet.write_string(row, col+2, text.translate(None, '0123456789')) #App Sugg - Edited
            worksheet.write_number(row, col+3, int(len(text)))  #Length
            worksheet.write_string(row, col+4, "Google")  #Source
            worksheet.write_string(row, col+5, "Google-Sugg")  #Type

        except UnicodeEncodeError:
            print ele
            print text
            print "Error occured!"
        finally:
            row = row + 1

    #-----------------------------------BING------------------------------------------

    bing_url = "https://www.bing.com/search?q="+searchterm

    driver.get(bing_url)
    # Get all the -----PAGE TITLES
    for ele in driver.find_elements_by_css_selector('a .b_algo'):
        text= str(getValueFromPage(ele,"text")).strip()
        f.write((text + '\n'))
        col = 1
        worksheet.write_string(row, col, Search_Term)
        worksheet.write_string(row, col + 1, text)  # App Sugg
        # 'Remove all numbers in edited string'
        worksheet.write_string(row, col + 2, text.translate(None, '0123456789'))  # App Sugg - Edited
        worksheet.write_number(row, col + 3, int(len(text)))  # Length
        worksheet.write_string(row, col + 4, "Bing")  # Source
        worksheet.write_string(row, col + 5, "Bing-Page-Title")  # Type
        row = row + 1

    "---------------------BING-IMAGE-SEARCH----------------------"
    bing_url = "https://www.bing.com/images/search?q="+searchterm
    driver.get(bing_url)
    scroll_to_bootom()
    # Get all the -KEYWORD SUGGESTIONS
    for ele in driver.find_elements_by_css_selector('.suggestion-item'):
        text = str(ele.get_attribute("title")).replace("Search for: ", "").strip()
        href = str(ele.get_attribute("href")).strip()
        f.write((text + '\n'))
        col = 1
        worksheet.write_string(row, col, Search_Term)
        worksheet.write_string(row, col + 1, text)  # App Sugg
        # 'Remove all numbers in edited string'
        worksheet.write_string(row, col + 2, text.translate(None, '0123456789'))  # App Sugg - Edited
        worksheet.write_number(row, col + 3, int(len(text)))  # Length
        worksheet.write_string(row, col + 4, "Bing")  # Source
        worksheet.write_string(row, col + 5, "Bing-Sugg")  # Type
        worksheet.write_url(row, col + 6, href)  # HREF
        row = row + 1

    # Get all the -TOP KEYWORD SUGGESTIONS
    for ele in driver.find_elements_by_css_selector('div.card>a'):
        text = str(ele.get_attribute("title")).replace("Search for: ", "").strip()
        href = str(ele.get_attribute("href")).strip()
        f.write((text + '\n'))
        col = 1
        worksheet.write_string(row, col, Search_Term)
        worksheet.write_string(row, col + 1, text)  # App Sugg
        # 'Remove all numbers in edited string'
        worksheet.write_string(row, col + 2, text.translate(None, '0123456789'))  # App Sugg - Edited
        worksheet.write_number(row, col + 3, int(len(text)))  # Length
        worksheet.write_string(row, col + 4, "Bing")  # Source
        worksheet.write_string(row, col + 5, "Bing-Top-Sugg")  # Type
        worksheet.write_url(row, col + 6, href)  # Type
        row = row + 1


    # close the file finally
    f.close()

workbook.close()
driver.quit()
# Play Windows exit sound.
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)


