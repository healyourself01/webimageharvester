# README #

Credentials saved to file: [C:\Users\Ajay\AppData\Roaming\gcloud\application_default_credentials.json]

This README would normally document whatever steps are necessary to get your application up and running.

### Web Image Harvester ###

* Quick summary
Take Keywords --> Find the Domains to crwal-->Download-->Validate-->Transform-->Index
* Version:0.01
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

##Detailed Description and Architecture##
###WEBROBOT00 - prepare domains to crawl###
1. Read the list of keywords from "keyowrds_web" kind.
2. Search web using google/bing and get the list of domains to crawl
3. Save the list of domains in "starturl_web" kind.
4. use the domain name as key for "starturl_web" kind, keyword = keyword from "keyowrds_web" kind.
5. Set WEBROBOT00_STATUS = True, WEBROBOT01_STATUS = False ,etc for upto WEBROBOT10_STATUS
###WEBROBOT01 - fetch the image URLs to download###
1. Read the list of domains from "starturl_web" kind with WEBROBOT01_STATUS = False. start the loop.
2. use google image search on the domain.
3. URL = image URL
4. key = domain+image name
5. Set WEBROBOT01_STATUS = True, WEBROBOT01_date = curr date
6. keyword = keyword from "starturl_web" kind.
7. tags = search tags that you can gather from the page.
8. save data in "imagelib_web" kind.

###WEBROBOT02 - download images from URLs###
1. loop through "imagelib_web" kind with WEBROBOT02_STATUS = False .
2. download the image to the given path in local drvie.
3. set WEBROBOT02_STATUS = True, WEBROBOT02_date = curr date, image_path_local = full drive path
###WEBROBOT03 - validate images###
1. loop through "imagelib_web" kind with WEBROBOT03_STATUS = False .
2. do logo validation, is it a registed logo?
3. do watermark validation, does it have water mark?
4. OCR validation, does the image hase any text?
5. face validation, does the image has any visible/recognizable face directly? 
6. celeb validation, does the image has any celeb?
7. movie validation, does the imae has any movie posters/stills?

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

###BING IMAGE SEARCH URL Parms
https://www.bing.com/images/search?&q=Cool+Short+Hairstyles+for+Men
&qft=+filterui:imagesize-custom_300_500+filterui:aspect-tall+filterui:photo-photo&FORM=R5IR22


