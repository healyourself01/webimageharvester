"""
Loop through app_names.xlsx file and download images using bing image search

"""
import xlsxwriter
from openpyxl import load_workbook
import ROBOT02
from ROBOT02 import download_images_bing, close_browser
import winsound

wb = load_workbook(filename='app_names.xlsx')
ws = wb['APP_NAME_SUGG_SH01']
# wb.save('app_names.xlsx')
OUTPUT_DIR = "D:\Andomeda Robot\IMAGE_LIB_WEB"
# for row in ws.iter_rows(min_row=1, max_col=10, max_row=2):
for row in ws.iter_rows(range_string="B2:H118"):
    source = str(row[4].value).replace(" ","")
    keyword = str(row[0].value)
    if source == "Bing":
        app_name = row[1].value
        app_url = row[6].value
        print app_name
        download_images_bing(outdir=OUTPUT_DIR, word=app_name, minHight=500, minWidth=300, img_type="photo", img_layout="tall", maxImages = 400)
        winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
    # for cell in row:
    #     print(cell.value)

close_browser()
# Play Windows exit sound.
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)
winsound.PlaySound("SystemExit", winsound.SND_ALIAS)


quit()