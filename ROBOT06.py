from pynput import keyboard
import os, PIL
from PIL import Image
from getch import getch, pause
import shutil

IMAGE_FILE_EXTENSIONS = ('.jpg', '.jpeg', ".png")
KEY_PRESSED = keyboard.Key.esc

def on_press(key):
    try:
        print('alphanumeric key {0} pressed'.format(
            key.char))
    except AttributeError:
        print('special key {0} pressed'.format(
            key))

def on_release(key):
    # print('{0} released'.format(
    #     key))
    global KEY_PRESSED
    KEY_PRESSED = key
    return False


def create_dir(dir_temp):
    path = os.path.join(DIR_BASE, dir_temp)
    if not os.path.isdir(path):
        os.mkdir(path)
"""
------------------------------------------------------------------
is_image(filename)
checks whether the file is image and returns True or False
------------------------------------------------------------------
"""


def is_image(filename):
    if os.path.splitext(filename)[1].lower() in IMAGE_FILE_EXTENSIONS:
        return True
    else:
        return False

"""
------------------------------------------------------------------
move_valid_images_to_set(item_count, set_dir)
checks whether the file is image and returns True or False
------------------------------------------------------------------
"""


def move_valid_images_to_set(item_count, set_dir):
    file_count = 0
    for item in os.listdir(os.path.join(DIR_BASE, VALID_DIR_STRING)):
        if is_image(item):
            if file_count >= item_count:
                break
            file_count = file_count + 1
            source = os.path.join(DIR_BASE, VALID_DIR_STRING,item)
            dest = os.path.join(DIR_BASE, set_dir,item)
            shutil.move(source, dest)
    return



DIR_BASE = "C://Users//Ajay//Google Drive//production_robot_apps//Simple_Mehndi_Designs"
ORIGINAL_DIR_STRING = "01_original_images"
VALID_DIR_STRING = "02_valid_images"
WATERMARK_DIR_STRING = "03_watermark_images"
TEXT_DIR_STRING = "03_text_images"
FACE_DIR_STRING = "03_face_images"
APP_NAME = "Simple_Mehndi_Designs"
SET1_STR = "set_1"
SET2_STR = "set_2"
SET3_STR = "set_3"
SET4_STR = "set_4"

# --------------------------------------------------------------------------------------------
# Crates the set directories by distributing the valid images
# --------------------------------------------------------------------------------------------

create_dir(SET1_STR)
create_dir(SET2_STR)
create_dir(SET3_STR)
create_dir(SET4_STR)

valid_images_list = os.listdir(os.path.join(DIR_BASE, VALID_DIR_STRING))

total_images = len(valid_images_list)

# Change the current dir
os.chdir(os.path.join(DIR_BASE, VALID_DIR_STRING))
images_per_set = round(total_images/4.0)
move_valid_images_to_set(images_per_set, SET1_STR)
move_valid_images_to_set(images_per_set, SET2_STR)
move_valid_images_to_set(images_per_set, SET3_STR)
move_valid_images_to_set(images_per_set, SET4_STR)

quit()


